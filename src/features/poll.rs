/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

use std::collections::HashMap;

use serenity::model::channel::{Message, ReactionType};

use crate::events::{Context, EventResult, Plugin, Event, CommandMetadata, Command};

const EMOJI: [&str; 20] = ["0️⃣", "1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣", "🔟", "🇦", "🇧", "🇨", "🇩", "🇪", "🇫", "🇬", "🇭", "🇮"];

async fn poll(ctx: Context, cmd: Command) -> EventResult {
    let choices = &cmd.arguments["choice"].values;

    if choices.len() > EMOJI.len() {
        return Err("Too many choices".into());
    }

    let title = cmd.arguments["title"].values[0].as_str().unwrap();

    let sent_msg = cmd.msg.channel_id.send_message(&ctx.c.http, |m| {
        m.add_embed(|e| {
            e
                .title(&format!("📋 Poll: {}", title))
                .description({
                    let mut s = String::new();
                    let mut i = 0;
                    for choice in choices {
                        s.push_str(&format!("{} {}\n", EMOJI[i], choice.as_str().unwrap()));
                        i += 1;
                    }
                    s
                })
                .footer(|f| {
                    f
                        .text(&cmd.msg.author.tag())
                        .icon_url(&cmd.msg.author.avatar_url().unwrap_or_default())
                })
        })
    }).await?;

    for i in 0..choices.len() {
        sent_msg.react(&ctx.c.http, ReactionType::Unicode(EMOJI[i].to_string())).await?;
    }

    Ok(())
}

async fn message_create(ctx: Context, msg: Message) -> EventResult {
    if msg.content.starts_with("+poll") {
        for reaction in ["👍", "🤷‍♀️", "👎"] {
            msg.react(&ctx.c.http, ReactionType::Unicode(reaction.to_string())).await?;
        }
    }

    Ok(())
}

pub fn register() -> Plugin {
    Plugin {
        events: vec![
            Event::MessageCreate(Box::new(move |context, message| Box::pin(message_create(context, message)))),
        ],
        commands: HashMap::from([
            ("poll".to_string(), cmdparse::Node::Command(cmdparse::Command {
                arguments: HashMap::from([
                    ("title".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(0),
                        value: cmdparse::Value::String,
                        default: None,
                        repeating: cmdparse::Repeat::False,
                        required: true,
                    }),
                    ("choice".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(1),
                        value: cmdparse::Value::String,
                        default: None,
                        repeating: cmdparse::Repeat::True,
                        required: true,
                    }),
                ]),
                subcommands: HashMap::new(),
                parse_negative_numbers: false,
                metadata: Box::new(CommandMetadata {
                    callback: Some(Box::new(move |context, command| Box::pin(poll(context, command)))),
                }),
            })),
        ]),
    }
}
